package ru.t1.vlvov.tm.api.service;

import org.jetbrains.annotations.NotNull;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

public interface IConnectionService {

    @NotNull
    EntityManagerFactory factory();

    @NotNull
    EntityManager getEntityManager();

}
