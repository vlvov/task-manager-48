package ru.t1.vlvov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.vlvov.tm.dto.model.TaskDTO;

import java.util.List;

public interface ITaskRepositoryDTO extends IUserOwnedRepositoryDTO<TaskDTO> {

    @Nullable
    List<TaskDTO> findAllByProjectId(@NotNull final String projectId);

    @Nullable
    List<TaskDTO> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId);

}
