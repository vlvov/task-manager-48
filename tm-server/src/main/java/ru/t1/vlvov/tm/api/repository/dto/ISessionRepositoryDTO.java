package ru.t1.vlvov.tm.api.repository.dto;

import ru.t1.vlvov.tm.dto.model.SessionDTO;

public interface ISessionRepositoryDTO extends IUserOwnedRepositoryDTO<SessionDTO> {
}
